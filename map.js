let arr = require('./items.js');

 
function map(val,cb) {
    const res=[]
    if(val.length==0) {
        return [];
    }
    for(i=0;i<val.length;i++) {
        res.push(cb(val[i],i))
    }
    return res;
}

module.exports = map;

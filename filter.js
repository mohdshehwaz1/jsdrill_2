
function filter(val,cb) {
    const res=[]
    if(val.length==0) {
        return [];
    }
    for(i=0;i<val.length;i++) {
        if(cb(val[i],i)) {
            res.push(val[i])
        }
    
    }
    return res;
}

module.exports = filter;

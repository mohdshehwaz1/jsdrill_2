function find(val,cb) {
    const res=[]
    if(val.length==0) {
        return [];
    }
    for(i=0;i<val.length;i++) {
        if(cb(val[i],i)) {
            return val[i];
        }
    }
    return undefined;
}

module.exports = find;

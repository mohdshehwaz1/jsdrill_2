function flatten(elements) {
    let res=[]
    if(elements.length==0) {
        return []
    }
    for(i=0;i<elements.length;i++) {
        if(Array.isArray(elements[i])) {
            res=res.concat(flatten(elements[i]));
        }
        else{
            res.push(elements[i]);
        }
    }
    return res;
}
module.exports = flatten;

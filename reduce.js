function reduce(val,fun) {
    let res = 0
    if(val.length==0) {
        return res;
    }
    for(i=0;i<val.length;i++)
    {
        res=res+ (fun(val[i],i));
    }
    return res;

}
module.exports = reduce;
